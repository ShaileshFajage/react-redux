import "./App.css";
import CakeContainer from "../src/compoenents/CakeContainer";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import CakeContainerHooks from './compoenents/CakeContainerHooks'
import Container from "./compoenents/Container";
import NewCakeContainer from "./compoenents/NewCakeContainer";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        {/* <CakeContainer />
        <CakeContainerHooks/> */}
        <Container/>
        <NewCakeContainer/>
      </div>
    </Provider>
  );
}

export default App;
