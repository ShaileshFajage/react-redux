import { USER_FETCH_REQUEST, USER_FETCH_SUCCESS, USER_FETCH_FAILURE } from "./userType";

const initialState = {
    loading : false,
    users : [],
    error : ''
}

const userReducer = (state=initialState, action) => {
    switch(action.type)
    {
        case USER_FETCH_REQUEST : return {
            ...state,
            loading : true
        }
        case USER_FETCH_SUCCESS : return {
            ...state,
            loading : false,
            users : action.payload
        }
        case USER_FETCH_FAILURE : return {
            ...state,
            error : action.payload
        }
        default : return state
    }
}

export default userReducer