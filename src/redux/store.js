import axios from 'axios'
import {createStore} from 'redux'
import cakeReducer from './cake/cakeReducer'
import icecreamReducer from './icecream/icecreamReducer'
import { combineReducers } from 'redux'
import logger from 'redux-logger'
import { applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import userReducer from './user/userReducer'
import { userFetchRequest, userFetchSuccess, userFetchFailure } from './user/userAction'
import thunk from 'redux-thunk'


const reducer = combineReducers({
    cake : cakeReducer,
    icecream : icecreamReducer,
    user : userReducer
})

// export const store = createStore(reducer, applyMiddleware(logger))

export const fetchUsers = () =>{
    return (dispatch) => {
        dispatch(userFetchRequest)
        axios.get('https://jsonplaceholder.typicode.com/users').then((res)=>{
            const users = res.data
            dispatch(userFetchSuccess(users))
        })
        .catch((error)=>{
            dispatch(userFetchFailure(error.message))
        })
    }
}

export const store = createStore(reducer, composeWithDevTools(applyMiddleware(logger, thunk)))

