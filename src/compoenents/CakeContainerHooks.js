import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {buyCake} from '../redux/cake/cakeAction'

function CakeContainerHooks() {

    const cakes = useSelector(state=>state.noOfCakes)
    const dispatch = useDispatch()
  return (
    <div>
        <h1>No of cakes {cakes}</h1>
        <button onClick={()=>{dispatch(buyCake())}}>Get Cake</button>
    </div>
  )
}

export default CakeContainerHooks