import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { fetchUsers } from '../redux/store'

function UserContainer(props) {

    useEffect(()=>{
        fetchedUsers()
    }, [])

  return (
    <div>
        {userData.loading ? <h2>...Loading</h2> : (user.error ? <h2>Something went wrong</h2> : <h2>userData.users[0].name</h2> )}
    </div>
  )
}

const mapStateToProps = state =>{
    return {
        userData : state.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchUsers : () => dispatch(fetchUsers())
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (UserContainer)