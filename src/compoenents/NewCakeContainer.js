
import React from 'react'
import { useState } from 'react'
import { buyCake } from '../redux/cake/cakeAction'
import { connect } from 'react-redux'

function NewCakeContainer(props) {
    
    const [number, setNumber] = useState()

  return (
    <div>
        <br/>
        <input value={number} onChange={(e)=>setNumber(e.target.value)}/>
        <button onClick={props.dispatch(number)}>Buy {number} cakes</button>
        <h1>{props.noOfCakes}</h1>
    </div>
  )
}

const mapStateToProps = state =>{
    return {
        noOfCakes : state.cake.noOfCakes
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatch : (number) => {dispatch(buyCake(number))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (NewCakeContainer)