import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { buyCake } from '../redux/cake/cakeAction'
import { buyIcecream } from '../redux/icecream/icecreamAction'

function Container() {
    const cakes = useSelector(state=>state.cake)
    const icecream = useSelector(state=>state.icecream)

    const dispatch = useDispatch()
  return (
    <div>
        <h1>Cakes {cakes.noOfCakes}</h1>
        <h1>Icecream {icecream.noOfIcecream}</h1>
        <button onClick={()=>dispatch(buyCake())}>Buy Cake</button>
        <button onClick={()=>dispatch(buyIcecream())}>Buy Icecream</button>
    </div>
  )
}

export default Container